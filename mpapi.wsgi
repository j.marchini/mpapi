#!/usr/bin/env python3

import os
import sys

cur_path = os.path.dirname(os.path.realpath(__file__))

sys.path.insert(0,cur_path)
sys.path.insert(0,cur_path+"/venv/lib/python%s/site-packages/" % sys.version[:3])

from base_app import app
import dolibarrWAPI

application = app

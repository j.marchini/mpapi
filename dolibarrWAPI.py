#! /usr/bin/env python3
"""
Dolibarr workaround API
completes Dolibarr standard API to work around bugs and missing stuffs

Example :

@app.route(base_url + 'direct_sql/<value>')
@api_response
def direct_sql(value):
     slot = _get_slot(conf_file)
     _sql_run(slot, "insert into toto values (40,%s)", (value,))
     # return _sql_run(e, "update toto set bar = %(bar)s where foo = 30", request.args)
     # return _sql_select(e, 'select count(*) from toto where foo = 20')
     # _sql_run(e, "update toto set bar = %(bar)s where foo >= 20", request.args)
     # return _sql_run(e, 'select count(*) from toto where foo >= %(a)s', {'a': 10})
     return _sql_run(slot, 'select * from toto')

"""


from flask import request
from base_app import app, api_response, _sql_run, _get_slot, _key_status, _status, os_cur_path
import json

base_url = '/dolibarrWAPI/'
version = '1.0'
conf_file = os_cur_path + '/dolibarrWAPI.conf.yml'


@app.route(base_url + 'status')
def status():
    return _status(version)


@app.route(base_url + 'key_status')
def key_status():
    return _key_status(conf_file)


@app.route(base_url + 'describe/<table>')
@api_response
def describe(table):
    """
    describe table
    :param table:
    :return:
    """
    sql_request = """ select column_name, data_type, character_maximum_length, 
                    is_nullable, column_default, ordinal_position 
                    from INFORMATION_SCHEMA.COLUMNS 
                    where table_name = %(table)s """
    slot = _get_slot(conf_file)
    return _sql_run(slot, sql_request, {'table': table})


@app.route(base_url + 'facture/update_ref_client')
@api_response
def update_ref_client():
    """
    updates an invoice ref field args = {'id': int, 'ref_value': str}
    :return:
    """
    slot = _get_slot(conf_file)
    return _sql_run(slot,
                    """ update 
                            llx_facture 
                        set tms = current_timestamp, 
                            ref_client = %(ref_value)s 
                        where 
                            rowid = %(id)s """,
                    request.args)


@app.route(base_url + 'thirdparty/touch/<rowid>')
@api_response
def thirdparty_touch(rowid):
    """
    set tms field (datetime update) to current server's current date time
    :return:
    """
    slot = _get_slot(conf_file)
    return _sql_run(slot, 'update llx_societe set tms = current_timestamp where rowid = %(rowid)s', {'rowid': rowid})


if __name__ == '__main__':
    with app.test_request_context('/?id=25&ref_value="et voila"', headers={'Api-Key': 'pg'}, data={}):
        r = update_ref_client()
        print(json.dumps(r.json, indent=2))

from flask import Flask, jsonify, request
import traceback
import logging
import logging.handlers
import yaml
import psycopg2
# http://initd.org/psycopg/docs/
import MySQLdb
# https://mysqlclient.readthedocs.io/
import os
import functools


def init_app_logger(_app):
    _app.logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_log_handler = logging.handlers.RotatingFileHandler(__file__+'.log', maxBytes=100_000, backupCount=1)
    file_log_handler.setFormatter(formatter)
    file_log_handler.setLevel(logging.INFO)
    _app.logger.addHandler(file_log_handler)


app = Flask(__name__)
init_app_logger(app)

os_cur_path = os.path.dirname((os.path.realpath(__file__)))


@app.route('/')
def base_index():
    return "It's working\n"


api_key_token = 'Api-Key'


def conf_slot(api_key, conf_file):
    """
    retrieves configuration slot matching a key
    :param api_key:
    :param conf_file:
    :return:
    """
    with open(conf_file, 'r') as stream:
        c = yaml.load(stream)
        for i, item in enumerate(c):
            try:
                if item['api_key'] == api_key:
                    return item
            except KeyError:
                app.logger.error('Configuration item {0} : {1} not found'.format(i, api_key_token))
                raise Exception('Configuration error')
    return None


def _get_slot(conf_file):
    """
    retrieves database access slot bound to an api key
    :param conf_file:
    :return:
    """
    k = dict(request.headers).get(api_key_token)
    if k is None:
        raise Exception('API key not found in headers')
    db_slot = conf_slot(k, conf_file)
    if db_slot is None:
        raise Exception('API key not valid')
    return db_slot


# postgres facilities
def _pg_connect(db_slot):
    return psycopg2.connect(
        host=db_slot.get('db_host', '127.0.0.1'),
        dbname=db_slot['db_name'],
        user=db_slot['db_user'],
        password=db_slot['db_password'])


def _pg_run(db_slot, sql_request, sql_params=None):
    with _pg_connect(db_slot) as connection:
        with connection.cursor() as cursor:
            cursor.execute(sql_request, sql_params)
            rowcount = cursor.rowcount
            result_set = []
            try:
                for record in cursor:
                    result_set.append(record)
            except psycopg2.ProgrammingError:
                pass
        connection.commit()
    return {'rowcount': rowcount, 'result_set': result_set}


# mysql-mariadb facilities
def _my_connect(db_slot):
    return MySQLdb.connect(
        host="localhost",
        user=db_slot['db_user'],
        passwd=db_slot['db_password'],
        db=db_slot['db_name'])


def _my_run(db_slot, sql_request, sql_params=None):
    connection = _my_connect(db_slot)
    try:
        cursor = connection.cursor()
        try:
            rowcount = cursor.execute(sql_request, sql_params)
            result_set = []
            for record in cursor.fetchall():
                result_set.append(record)
        finally:
            cursor.close()
        connection.commit()
    finally:
        connection.close()
    return {'rowcount': rowcount, 'result_set': result_set}


# database facilities
def _db_connection_test(db_slot):
    if db_slot['db_kind'] == 'postgres':
        return _pg_connect(db_slot)
    if db_slot['db_kind'] == 'mysql':
        return _my_connect(db_slot)
    return None


def _sql_run(db_slot, sql_request, sql_params=None):
    if db_slot['db_kind'] == 'postgres':
        return _pg_run(db_slot, sql_request, sql_params)
    if db_slot['db_kind'] == 'mysql':
        return _my_run(db_slot, sql_request, sql_params)


# API facilities
def _success(result):
    result = {"ok": True, 'result': result}
    return jsonify(result)


def _no_success(e):
    app.logger.exception('No success {0}'.format(e))
    return jsonify({"ok": False, "Error": str(e), "Traceback": traceback.format_exc()})


def api_response(func):
    """
    decorator, encapsulate API calls to make json response and log error
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
            return _success(result)
        except Exception as e:
            return _no_success(e)
    return wrapper


def _status(version):
    """
    ping API, returns and logs all request parts
    :return:
    """
    app.logger.info('Status : ')
    result = {
        'Api Name': __name__,
        'Api File': __file__,
        'Version': version,
        'Method': request.method,
        'Url': request.url,
        'Query String': str(request.query_string),
        'Args': request.args,
        'Cookies': request.cookies,
        'Form': request.form,
        'headers': dict(request.headers),
    }
    app.logger.info(result)
    return _success(result)


@api_response
def _key_status(conf_file):
    """
    test key presence, configuration, and db connection if any
    :return:
    """
    e = _get_slot(conf_file)
    db = _db_connection_test(e)
    if db is None:
        return {'Api Key': e['api_key']}
    return {
        'Api Key': e['api_key'],
        'Server': e['db_kind'],
        'Database name': e['db_name'],
        'User Name': e['db_user']}
